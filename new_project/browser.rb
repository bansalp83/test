require 'rubygems'
require 'data_mapper'
require 'dm-mysql-adapter'
require 'sinatra'
require 'haml'

DataMapper::Logger.new($stdout, :debug)

DataMapper.setup(
	:default,
        :adapter => "mysql",
        :host => "localhost",
        :username => "root",
        :password => "pooja",
        :database => "mydb"
)

DataMapper::Property.auto_validation(false)
DataMapper::Property.required(false)

class ProjectLead 
	include DataMapper::Resource
	property :id, 	Serial 	# Required in all DM classes
	property :leader_name, String, :required => true
        has n, :interns
        
end

class Intern
	include DataMapper::Resource
	property :id, 	Serial 	# Required in all DM classes
	property :name, String, :required => true
	property :college, String, :required =>true
	property :joinedOn, Date, :required=>true
        belongs_to :project_lead
end

DataMapper.auto_migrate!
DataMapper.finalize


['Gaurav','David','Vanshi','Jimmy'].each do |name|
	ProjectLead.create(:leader_name => name)
end
[['Pooja',1], ['Saurabh',2], ['Sahil',3], ['Priya',4]].each do |name|
	Intern.create(:name => name[0],:college=>'NIT,Jaipur',:joinedOn=>'27-05-2013',:project_lead_id=>name[1])

end

get '/intern' do
@interns=Intern.all 
haml :intern
end

post '/intern/insert' do
@v1=ProjectLead.first(:leader_name=>params[:leader]).id
Intern.create(:name => params[:name],:college=>params[:college],:joinedOn=>params[:joinedOn],:project_lead_id=>@v1)
@v2=params[:name]
end

post '/intern/delete' do
@v=Intern.first(:name=>params[:name])
@v1=@v.name
@v.destroy
"#{@v1}"
end

post '/intern/show' do
@var=Intern.first(:name=>params[:name])
@v1=ProjectLead.first(:id=>@var.project_lead_id)
"#{@var.name} from #{@var.college} joined internship programme on #{@var.joinedOn} under the guidance of #{@v1.leader_name}"

end









